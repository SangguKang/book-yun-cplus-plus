#include "computer.h"

Computer::Computer(char *name)
{
    strcpy(owner, name);
}

Computer::~Computer()
{
    //dtor
}

NotebookComp::NotebookComp(char *name, int initChag)
    : Computer(name), Battery(initChag)
{
    //
}

TabletNotebook::TabletNotebook(char *name, int initChag, char *pen)
    : NotebookComp(name, initChag)
{
    strcpy(regstPenModel, pen);
}

////////////////////////////////////////////////////////////////////
void Computer::Calculate()
{
    cout << "Calculate contents of your request." << endl;
}

void NotebookComp::Charging()
{
    Battery += 5;
}

void NotebookComp::UseBattery()
{
    Battery -= 1;
}

void NotebookComp::MovingCal()
{
    if(GetBateryInfo() < 1) {
        cout << "Need to charge." << endl;
        return;
    }

    cout << "Moving ";
    Calculate();
    UseBattery();
}

int NotebookComp::GetBateryInfo()
{
    return Battery;
}

void TabletNotebook::Write(char *penInfo)
{
    if(GetBateryInfo() < 1) {
        cout << "충전이 필요합니다." << endl;
        return;
    }

    if(strcmp(regstPenModel, penInfo) != 0) {
        cout << "등록된 펜이 아닙니다.";
        return;
    }

    cout << "필기 내용을 처리합니다." << endl;
    UseBattery();
}


