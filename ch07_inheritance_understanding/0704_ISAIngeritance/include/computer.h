#ifndef COMPUTER_H
#define COMPUTER_H

#include <iostream>
#include <cstring>
using namespace std;

class Computer
{
    public:
        Computer(char *name);
        virtual ~Computer();

        void Calculate();

    protected:

    private:
        char owner[50];
};

class NotebookComp : public Computer
{
private:
    int Battery;

public:
    NotebookComp(char *name, int initChag);

    void Charging();
    void UseBattery();
    void MovingCal();
    int GetBateryInfo();
};

class TabletNotebook : public NotebookComp
{
private:
    char regstPenModel[50];

public:
    TabletNotebook(char *name, int initCharg, char *pen);

    void Write(char *penInfo);
};




#endif // COMPUTER_H
