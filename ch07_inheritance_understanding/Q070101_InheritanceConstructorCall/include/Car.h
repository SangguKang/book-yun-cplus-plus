#ifndef CAR_H
#define CAR_H

#include <iostream>
using namespace std;

class Car
{
    public:
        Car(int gas);
        virtual ~Car();

        int GetGasGauge();

    protected:

    private:
        int gasolineGauge;
};

class HybridCar : public Car
{
private:
    int electricGauge;
public:
    HybridCar(int gas, int elec);

    int GetElecGauge();
};

class HybridWaterCar : public HybridCar
{
private:
    int waterGuage;
public:
    HybridWaterCar(int gas, int elec, int water);

    void ShowCurrentGuage();
};






#endif // CAR_H
