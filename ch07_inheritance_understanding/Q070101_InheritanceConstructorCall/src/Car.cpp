#include "Car.h"

Car::Car(int gas) : gasolineGauge(gas)
{
    //ctor
}

Car::~Car()
{
    //dtor
}

HybridCar::HybridCar(int gas, int elec) : Car(gas), electricGauge(elec)
{

}

HybridWaterCar::HybridWaterCar(int gas, int elec, int water) : HybridCar(gas, elec), waterGuage(water)
{

}

int Car::GetGasGauge()
{
    return gasolineGauge;
}

int HybridCar::GetElecGauge()
{
    return electricGauge;
}

void HybridWaterCar::ShowCurrentGuage()
{
    cout << "�ܿ� ���ָ�: " << GetGasGauge() << endl;
    cout << "�ܿ� ���ⷮ: " << GetElecGauge() << endl;
    cout << "�ܿ� ���ͷ�: " << waterGuage << endl;
}

