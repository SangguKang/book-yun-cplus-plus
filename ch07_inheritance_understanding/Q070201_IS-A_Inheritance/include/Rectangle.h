#ifndef RECTANGLE_H
#define RECTANGLE_H

#include <iostream>
using namespace std;

class Rectangle
{
    public:
        Rectangle(int wid, int hei);
        virtual ~Rectangle();

        void ShowAreaInfo();

    protected:

    private:
        int width;
        int height;
};

class Square : public Rectangle
{
private:
    //
public:
    Square(int side);
};

#endif // RECTANGLE_H
