#include "UnivStudent.h"

UnivStudent::UnivStudent(char * myname, int myage, char * mymajor) :
    Person(myage, myname)
{
    strcpy(major, mymajor);
}

UnivStudent::~UnivStudent()
{
    //dtor
}

void UnivStudent::WhoAreYou() const
{
    WhatYourName();
    HowOldAreYou();
    cout << "My major is " << major << endl << endl;
}
