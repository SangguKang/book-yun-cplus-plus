#include "Person.h"

Person::Person(int myage, char * myname) : age(myage)
{
    strcpy(name, myname);
}

Person::~Person()
{
    //dtor
}

void Person::WhatYourName() const
{
    cout << "My name is " << name << endl;
}

void Person::HowOldAreYou() const
{
    cout << "I'm " << age << " years old" << endl;
}

