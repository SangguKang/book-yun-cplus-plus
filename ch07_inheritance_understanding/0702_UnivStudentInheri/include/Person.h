#ifndef PERSON_H
#define PERSON_H

#include <iostream>
#include <cstring>
using namespace std;

class Person
{
    public:
        Person(int myage, char * myname);
        virtual ~Person();

        void WhatYourName() const;
        void HowOldAreYou() const;

    protected:

    private:
        int age;
        char name[50];
};

#endif // PERSON_H
