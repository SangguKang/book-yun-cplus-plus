#ifndef UNIVSTUDENT_H
#define UNIVSTUDENT_H

#include "Person.h"

class UnivStudent : public Person
{
    public:
        //UnivStudent(char * myname, int myage, char * mymajor) : Person(myage, myname);
        UnivStudent(char * myname, int myage, char * mymajor);
        virtual ~UnivStudent();

        void WhoAreYou() const;

    protected:

    private:
        char major[50];
};

#endif // UNIVSTUDENT_H
