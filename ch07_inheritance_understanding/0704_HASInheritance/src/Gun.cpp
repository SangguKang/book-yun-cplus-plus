#include "Gun.h"

Gun::Gun(int bnum) : bullet(bnum)
{
    //ctor
}

Gun::~Gun()
{
    //dtor
}

Police::Police(int bnum, int bcuff)
    : Gun(bnum), handcuffs(bcuff)
{

}

Police::~Police()
{

}

void Gun::Shut()
{
    cout << "BBANG!" << endl;
    bullet--;
}

void Police::PutHandcuff()
{
    cout << "SNAP!" << endl;
    handcuffs--;
}

