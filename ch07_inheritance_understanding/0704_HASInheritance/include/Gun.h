#ifndef GUN_H
#define GUN_H

#include <iostream>
using namespace std;

class Gun
{
    public:
        Gun(int bnum);
        virtual ~Gun();

        void Shut();

    protected:

    private:
        int bullet;
};

class Police : public Gun
{
private:
    int handcuffs;

public:
    Police(int bnum, int bcuff);
    ~Police();

    void PutHandcuff();
};

#endif // GUN_H
