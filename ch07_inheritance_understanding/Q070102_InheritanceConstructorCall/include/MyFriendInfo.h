#ifndef MYFRIENDINFO_H
#define MYFRIENDINFO_H

#include <iostream>
#include <cstring>
using namespace std;

class MyFriendInfo
{
    public:
        MyFriendInfo(char* fname, int fage);
        virtual ~MyFriendInfo();

        void ShowMyFriendInfo();

    protected:

    private:
        char* name;
        int age;
};

class MyFriendDetailInfo : public MyFriendInfo
{
private:
    char* addr;
    char* phone;

public:
    MyFriendDetailInfo(char* fname, int fage, char* adr, char* pnum);
    ~MyFriendDetailInfo();

    void ShowMyFriendDetailInfo();
};
#endif // MYFRIENDINFO_H
