#include "MyFriendInfo.h"

MyFriendInfo::MyFriendInfo(char* fname, int fage) : age(fage)
{
    name = new char[strlen(fname) + 1];
    strcpy(name, fname);
}

MyFriendInfo::~MyFriendInfo()
{
    delete []name;
}

void MyFriendInfo::ShowMyFriendInfo()
{
    cout << "이름: " << name << endl;
    cout << "나이: " << age << endl;
}

MyFriendDetailInfo::MyFriendDetailInfo(char* fname, int fage, char* adr, char* pnum)
    : MyFriendInfo(fname, fage)
{
    addr = new char[strlen(adr) + 1];
    phone = new char[strlen(pnum) + 1];
    strcpy(addr, adr);
    strcpy(phone, pnum);
}

MyFriendDetailInfo::~MyFriendDetailInfo()
{
    delete []addr;
    delete []phone;
}

void MyFriendDetailInfo::ShowMyFriendDetailInfo()
{
    ShowMyFriendInfo();
    cout << "주소: " << addr << endl;
    cout << "전화: " << phone << endl;
}

