#include "MyFriendInfo.h"

int main()
{
    MyFriendDetailInfo fren1("김진성", 22, "충남 아산", "010-1234-0012");
    MyFriendDetailInfo fren2("이주성", 19, "경기 인천", "010-3333-3587");

    fren1.ShowMyFriendDetailInfo();
    fren2.ShowMyFriendDetailInfo();

    return 0;
}
