#ifndef SODERIVED_H
#define SODERIVED_H

#include "SoBase.h"

class SoDerived : public SoBase
{
    public:
        SoDerived(int n);
        virtual ~SoDerived();

    protected:

    private:
        int derivNum;
};

#endif // SODERIVED_H
