#ifndef SOBASE_H
#define SOBASE_H

#include <iostream>
using namespace std;

class SoBase
{
    public:
        SoBase(int n);
        virtual ~SoBase();

    protected:

    private:
        int baseNum;
};

#endif // SOBASE_H
