#include "SoDerived.h"

SoDerived::SoDerived(int n) : SoBase(n), derivNum(n)
{
    cout << "SoDerived(): " << derivNum << endl;
}

SoDerived::~SoDerived()
{
    cout << "~SoDerived(): " << derivNum << endl;
}
