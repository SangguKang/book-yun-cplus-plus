#include "SoBase.h"

SoBase::SoBase(int n) : baseNum(n)
{
    cout << "SoBase(): " << baseNum << endl;
}

SoBase::~SoBase()
{
    cout << "~SoBase(): " << baseNum << endl;
}
