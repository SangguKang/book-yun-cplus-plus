#ifndef BOOK_H
#define BOOK_H

#include <iostream>
#include <cstring>
using namespace std;

class Book
{
    public:
        Book(char* title, char* isbn, int value);
        virtual ~Book();

        void ShowBookInfo();

    protected:

    private:
        char* title;
        char* isbn;
        int price;
};

class EBook : public Book
{
private:
    char* DRMKey;

public:
    EBook(char* title, char* isbn, int value, char* key);
    ~EBook();

    void ShowEBookInfo();
};

#endif // BOOK_H
