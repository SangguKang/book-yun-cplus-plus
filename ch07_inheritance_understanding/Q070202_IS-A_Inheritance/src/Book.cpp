#include "Book.h"

Book::Book(char* title, char* isbn, int value) : price(value)
{
    this->title = new char[strlen(title) + 1];
    this->isbn = new char[strlen(isbn) + 1];
    strcpy(this->title, title);
    strcpy(this->isbn, isbn);
}

Book::~Book()
{
    delete []title;
    delete []isbn;
}

void Book::ShowBookInfo()
{
    cout << "제목: " << title << endl;
    cout << "ISBN: " << isbn << endl;
    cout << "가격: " << price << endl;
}

EBook::EBook(char* title, char* isbn, int value, char* key)
    : Book(title, isbn, value)
{
    DRMKey = new char[strlen(key) + 1];
    strcpy(DRMKey, key);
}

EBook::~EBook()
{
    delete []DRMKey;
}

void EBook::ShowEBookInfo()
{
    ShowBookInfo();
    cout << "인증키: " << DRMKey << endl;
}
