#ifndef SODERIVED_H
#define SODERIVED_H

#include "SoBase.h"

class SoDerived : public SoBase
{
    public:
        SoDerived();
        SoDerived(int n);
        SoDerived(int n1, int n2);
        virtual ~SoDerived();

        void ShowDerivData();

    protected:

    private:
        int derivNum;
};

#endif // SODERIVED_H
