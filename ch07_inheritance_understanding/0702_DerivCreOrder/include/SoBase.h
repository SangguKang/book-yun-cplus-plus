#ifndef SOBASE_H
#define SOBASE_H

#include <iostream>
using namespace std;

class SoBase
{
    public:
        SoBase();
        SoBase(int n);
        virtual ~SoBase();

        void ShowBaseData();

    protected:

    private:
        int baseNum;
};

#endif // SOBASE_H
