#include "SoBase.h"

SoBase::SoBase() : baseNum(20)
{
    cout << "SoBase()" << endl;
}

SoBase::SoBase(int n) : baseNum(n)
{
    cout << "SoBase(int n)" << endl;
}

SoBase::~SoBase()
{
    //dtor
}

void SoBase::ShowBaseData()
{
    cout << baseNum << endl;
}
