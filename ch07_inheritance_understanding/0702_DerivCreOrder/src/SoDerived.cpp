#include "SoDerived.h"

SoDerived::SoDerived() : derivNum(30)
{
    cout << "SoDerived()" << endl;
}

SoDerived::SoDerived(int n) : derivNum(n)
{
    cout << "SoDerived(int n)" << endl;
}

SoDerived::SoDerived(int n1, int n2) : SoBase(n1), derivNum(n2)
{
    cout << "SoDerived(int n1, int n2)" << endl;
}

SoDerived::~SoDerived()
{
    //dtor
}

void SoDerived::ShowDerivData()
{
    ShowBaseData();
    cout << derivNum << endl;
}
