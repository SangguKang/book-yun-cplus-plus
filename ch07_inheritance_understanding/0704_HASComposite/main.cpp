#include "Gunpolice.h"

int main()
{
    Police pman1(5, 3);
    pman1.Shut();
    pman1.PutHandcuff();

    Police pman2(0, 3);
    pman2.Shut();
    pman2.PutHandcuff();

    return 0;
}
