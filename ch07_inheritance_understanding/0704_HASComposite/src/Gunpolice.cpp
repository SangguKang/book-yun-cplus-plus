#include "Gunpolice.h"

Gun::Gun(int bnum) : bulet(bnum)
{
    //ctor
}

Gun::~Gun()
{
    //dtor
}

Police::Police(int bnum, int bcuff) : handcuffs(bcuff)
{
    if(bnum > 0)
        pistol = new Gun(bnum);
    else
        pistol = NULL;
}

Police::~Police()
{
    if(pistol != NULL)
        delete pistol;
}

void Gun::Shut()
{
    cout << "BBANG!" << endl;
    bulet--;
}

void Police::PutHandcuff()
{
    cout << "SNAP!" << endl;
    handcuffs--;
}

void Police::Shut()
{
    if(pistol==NULL)
        cout << "Hut BBANG!" << endl;
    else
        pistol->Shut();
}
