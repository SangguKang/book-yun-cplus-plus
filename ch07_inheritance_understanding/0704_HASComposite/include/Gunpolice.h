#ifndef GUN_H
#define GUN_H

#include <iostream>
using namespace std;

class Gun
{
    public:
        Gun(int bnum);
        virtual ~Gun();

        void Shut();

    protected:

    private:
        int bulet;
};

class Police
{
private:
    int handcuffs;
    Gun *pistol;

public:
    Police(int bnum, int bcuff);
    ~Police();

    void PutHandcuff();
    void Shut();
};


#endif // GUN_H
