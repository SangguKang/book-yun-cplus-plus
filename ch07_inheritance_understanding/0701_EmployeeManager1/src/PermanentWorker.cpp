#include "PermanentWorker.h"
#include <stdio.h>
#include <string.h>
#include <iostream>
using namespace std;

PermanentWorker::PermanentWorker(char* name, int money)
    : salary(money)
{
    strcpy(this->name, name);
}

PermanentWorker::~PermanentWorker()
{
    //dtor
}

int PermanentWorker::GetPay() const
{
    return salary;
}

void PermanentWorker::ShowSalaryInfo() const
{
    cout << "name: " << name << endl;
    cout << "salary: " << GetPay() << endl << endl;
}
