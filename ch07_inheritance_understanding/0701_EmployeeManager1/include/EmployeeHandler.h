#ifndef EMPLOYEEHANDLER_H
#define EMPLOYEEHANDLER_H

#include "PermanentWorker.h"

class EmployeeHandler
{
    public:
        EmployeeHandler();
        virtual ~EmployeeHandler();

        void AddEmployee(PermanentWorker* emp);
        void ShowAllSalaryInfo() const;
        void ShowTotalSalary() const;

    protected:

    private:
        PermanentWorker* empList[50];
        int empNum;
};

#endif // EMPLOYEEHANDLER_H
