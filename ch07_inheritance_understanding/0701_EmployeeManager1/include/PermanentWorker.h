#ifndef PERMANENTWORKER_H
#define PERMANENTWORKER_H


class PermanentWorker
{
    public:
        PermanentWorker(char* name, int money);
        virtual ~PermanentWorker();

        int GetPay() const;
        void ShowSalaryInfo() const;

    protected:

    private:
        char name[100];
        int salary;
};

#endif // PERMANENTWORKER_H
