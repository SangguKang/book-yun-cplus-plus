#include "Person.h"

Person::Person(char *myname)
{
    name = new char[strlen(myname) + 1];
    strcpy(name, myname);
}

Person::~Person()
{
    delete []name;
}

void Person::WhatYourName() const
{
    cout << "My name is " << name << endl;
}
