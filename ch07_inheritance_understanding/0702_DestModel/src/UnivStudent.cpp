#include "UnivStudent.h"

UnivStudent::UnivStudent(char * myname, char * mymajor) : Person(myname)
{
    major = new char[strlen(mymajor) + 1];
    strcpy(major, mymajor);
}

UnivStudent::~UnivStudent()
{
    delete []major;
}

void UnivStudent::WhoAreYou() const
{
    WhatYourName();
    cout << "My major is " << major << endl;
}
