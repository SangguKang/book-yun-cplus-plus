#ifndef PERSON_H
#define PERSON_H

#include <cstring>
#include <iostream>
using namespace std;

class Person
{
    public:
        Person(char * myname);
        virtual ~Person();

        void WhatYourName() const;

    protected:

    private:
        char * name;
};

#endif // PERSON_H
