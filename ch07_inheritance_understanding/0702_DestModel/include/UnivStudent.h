#ifndef UNIVSTUDENT_H
#define UNIVSTUDENT_H

#include "Person.h"

class UnivStudent : public Person
{
    public:
        UnivStudent(char * myname, char * mymajor);
        virtual ~UnivStudent();

        void WhoAreYou() const;

    protected:

    private:
        char * major;
};

#endif // UNIVSTUDENT_H
