#include "UnivStudent.h"

int main()
{
    UnivStudent st1("Kim", "Mathematics");
    st1.WhoAreYou();

    UnivStudent st2("Hong", "Physics");
    st2.WhoAreYou();

    return 0;
}
