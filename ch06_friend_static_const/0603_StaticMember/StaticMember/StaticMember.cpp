#include <iostream>
using namespace std;

class SoSimple
{
private:
    static int simObjCnt;
public:
    SoSimple()
    {
        simObjCnt++;                                        //마치 멤버변수인것처럼 접근이 가능하지만 그렇다고 static 변수를 멤버변수로 오해하면 안된다.
        cout << simObjCnt << "번째 SoSimple 객체" << endl;
    }
};
int SoSimple::simObjCnt = 0;                                //static 변수의 초기화 방법. 생성자가 아닌 클래스 외부에서 초기화해야 함.

class SoComplex
{
private:
    static int cmxObjCnt;
public:
    SoComplex()
    {
        cmxObjCnt++;
        cout << cmxObjCnt << "번째 SoComplex 객체" << endl;
    }
    SoComplex(SoComplex &copy)
    {
        cmxObjCnt++;
        cout << cmxObjCnt << "번째 SoComplex 객체 copy" << endl;
    }
};
int SoComplex::cmxObjCnt = 0;

int main(void)
{
    SoSimple sim1;
    SoSimple sim2;

    SoComplex cmx1;
    SoComplex cmx2 = cmx1;
    SoComplex();

    system("pause");
    return 0;
}

