#include <iostream>
#include <cstring>
using namespace std;

class Girl;     //Girl이라는 이름의 class의 이름임을 알림
                //kdebug: 정의가 뒤에 나오는 함수의 호출을 위해서 함수의 원형을 미리 선언하는 것과 같음.

class Boy
{
private:
    int height;
    friend class Girl;  //Girl class에 대한 friend 선언
                        //kdebug: 5행의 class 선언이 없어도 컴파일 된다. 여기에서 Girl이 class 이름임을 알리는 역할도 하기 때문이다.

public:
    Boy(int len) : height(len)
    {
        //
    }
    void ShowYourFriendInfo(Girl &frn);//kdebug: 아직 정의되지 않은 Girl이라는 class의 이름 등장. 컴파일이 가능한 이유는 앞서 5행에서 Girl이 class 이름임을 알렸기 때문.
};

class Girl
{
private:
    char phNum[20];
public:
    //Girl(char *num)
    Girl(const char *num)   //kdebug: 최근의 스탠다드는 명확하게 사용하기를 요구함.
    {
        //strcpy(phNum, num);   //kdebug: strcpy()는 버퍼 오버플로우의 위험성이 존재하기 때문에 사용하지 않는 것이 좋다. visual studio에서는 엄격히 금지하고 있다.
        strcpy_s(phNum, num); //kdebug: dest인 phNum이 배열로 선언되었기 때문에 별도의 size 지정 없이 사용 가능.
        //strcpy_s(phNum, 13, num);   //kdebug: visual studio에서는 size에 오류가 있을 경우 자동으로 에러를 내 준다.
    }
    void ShowYourFriendInfo(Boy &frn);
    friend class Boy;   //Boy class에 대한 friend 선언
};

void Boy::ShowYourFriendInfo(Girl &frn)
{
    cout << "Her phone number: " << frn.phNum << endl;
}

void Girl::ShowYourFriendInfo(Boy &frn)
{
    cout << "His height: " << frn.height << endl;
}

int main(void)
{
    Boy boy(170);
    Girl girl("010-1234-5678");

    boy.ShowYourFriendInfo(girl);
    girl.ShowYourFriendInfo(boy);

    system("pause");
    return 0;
}
