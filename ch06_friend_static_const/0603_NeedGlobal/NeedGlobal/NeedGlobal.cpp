#include <iostream>
using namespace std;

int simObjCnt = 0;  //extern variable
int cmxObjCnt = 0;  //extern variable

class SoSimple
{
public:
    SoSimple()
    {
        simObjCnt++;
        cout << simObjCnt << "��° SoSimple ��ü" << endl;
    }
};

class SoComplex
{
public:
    SoComplex()
    {
        cmxObjCnt++;
        cout << cmxObjCnt << "��° SoComplex ��ü" << endl;
    }
    SoComplex(SoComplex &copy)
    {
        cmxObjCnt++;
        cout << cmxObjCnt << "��° SoComplex ��ü(param)" << endl;
    }
};

int main(void)
{
    SoSimple sim1;
    cout << "-----------" << endl;//kdebug
    SoSimple sim2;
    cout << "-----------" << endl;//kdebug

    SoComplex com1;
    cout << "-----------" << endl;//kdebug
    SoComplex com2 = com1;
    cout << "-----------" << endl;//kdebug
    SoComplex();
    cout << "-----------" << endl;//kdebug

    system("pause");
    return 0;
}
