#include <iostream>
using namespace std;

class Point;    //Point가 클래스의 이름임을 선언
                //14, 15행을 컴파일하기 위해서 Point가 클래스의 이름임을 컴파일러에게 알려줘야 함.
class PointOP
{
private:
    int opcnt;
public:
    PointOP() : opcnt(0)
    {}

    Point PointAdd(const Point&, const Point&);
    Point PointSub(const Point&, const Point&);
    ~PointOP()
    {
        cout << "Operation times: " << opcnt << endl;
    }
};

class Point
{
private:
    int x;
    int y;
public:
    Point(const int &xpos, const int &ypos) : x(xpos), y(ypos)
    {}
    friend Point PointOP::PointAdd(const Point&, const Point&); //PointOP 클래스의 맴버함수 PointAdd와 PointSub에 대해 friend 선언을 하고 있다.
    friend Point PointOP::PointSub(const Point&, const Point&);
    friend void ShowPointPos(const Point&); //60행에 정의된 함수 ShowPointPos에 대해 friend 선언을 하고 있다.
};

Point PointOP::PointAdd(const Point& pnt1, const Point& pnt2)
{
    opcnt++;
    return Point(pnt1.x + pnt2.x, pnt1.y + pnt2.y); //Point 클래스의 friend로 선언되었기 때문에 Point 클래스의 private 멤버에 접근이 가능하다.
}

Point PointOP::PointSub(const Point& pnt1, const Point& pnt2)
{
    opcnt++;
    return Point(pnt1.x - pnt2.x, pnt1.y - pnt2.y);
}

int main(void)
{
    Point pos1(1, 2);
    Point pos2(2, 4);
    PointOP op;

    ShowPointPos(op.PointAdd(pos1, pos2));
    ShowPointPos(op.PointSub(pos2, pos1));

    system("pause");
    return 0;
}

void ShowPointPos(const Point& pos)
{
    cout << "x: " << pos.x << ", "; //Point 클래스의 friend로 선언되었기 때문에 Point 클래스의 private 멤버에 접근이 가능하다.
    cout << "y: " << pos.y << endl;
}
