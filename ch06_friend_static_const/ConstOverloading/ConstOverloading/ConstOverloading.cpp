#include <iostream>

using namespace std;

class SoSimple
{
private:
    int num;
public:
    SoSimple(int n) : num(n)
    {
        //
    }
    SoSimple& AddNum(int n)
    {
        num += n;
        return *this;
    }
    void SimpleFunc()
    {
        cout << "SimpleFunc: " << num << endl;
    }
    void SimpleFunc() const
    {
        cout << "const SimpleFunc: " << num << endl;
    }
};

void YourFunc(const SoSimple &obj)
{
    obj.SimpleFunc();
}

int main(void)
{
    SoSimple obj1(2);//일반 객체 생성
    const SoSimple obj2(7);//const 객체 생성

    obj1.SimpleFunc();//일반 객체를 대상으로 SimpleFunc 함수를 호출하면 19행의 일반 멤버함추가 호출된다.
    obj2.SimpleFunc();//const 객체를 대상으로 SimpleFunc 함수를 호출하면 23행의 const 멤버 함수가 호출된다.

    YourFunc(obj1);//29행에 정의된 YourFunc() 함수는 전달되는 인자를 참조자로, 그것도 const 참조자로 받는다. 따라서 참조자를 이용한 31행의 함수호출의 결과로 23행의 const 멤버함수가 호출된다.
    YourFunc(obj2);

    system("pause");
    return 0;
}
