#include <iostream>
#include <cstring>
using namespace std;

class Employee
{
private:
    char name[100];
public:
    Employee(char* pName);
    virtual ~Employee();
    void ShowYourName() const;
};

class PermanentWorkder : public Employee
{
private:
    int salary;
public:
    PermanentWorkder(char* pName, int nMoney);
    virtual ~PermanentWorkder();
    int GetPay() const;
    void ShowSalaryInfo() const;
};

class EmployeeHandler
{
private:
    Employee* empList[50];
    int empNum;
public:
    EmployeeHandler();
    virtual ~EmployeeHandler();
    void AddEmployee(Employee* emp);
    void ShowAllSalaryInfo() const;
    void ShowTotalSalary() const;
};

class TemporaryWorker : public Employee
{
private:
    int workTime;
    int payPerHour;
public:
    TemporaryWorker(char* pName, int nPay);
    virtual ~TemporaryWorker();
    void AddWorkTime(int nTime);
    int GetPay() const;
    void ShowSalaryInfo() const;
};

class SalesWorker : public PermanentWorkder
{
private:
    int salesResult;
    double bonusRatio;
public:
    SalesWorker(char* pName, int nMoney, double dRatio);
    ~SalesWorker();
    void AddSalesResult(int value);
    int GetPay() const;
    void ShowSalaryInfo() const;
};

/////////////////////////////////////////////////////
Employee::Employee(char* pName)
{
    strcpy(name, pName);
}
Employee::~Employee()
{
    //
}
void Employee::ShowYourName() const
{
    cout << "name: " << name << endl;
}
//----------------------------
PermanentWorkder::PermanentWorkder(char* pName, int nMoney) : Employee(pName), salary(nMoney)
{
    //
}
PermanentWorkder::~PermanentWorkder()
{
    //
}
int PermanentWorkder::GetPay() const
{
    return salary;
}
void PermanentWorkder::ShowSalaryInfo() const
{
    ShowYourName();
    cout << "salary: " << GetPay() << endl << endl;
}
//----------------------------
EmployeeHandler::EmployeeHandler() : empNum(0)
{
    //
}
EmployeeHandler::~EmployeeHandler()
{
    for(int i=0; i<empNum; i++)
        delete empList[i];
}
void EmployeeHandler::AddEmployee(Employee* emp)
{
    empList[empNum++] = emp;
}
void EmployeeHandler::ShowAllSalaryInfo() const
{
    /*
    for(int i=0; i<empNum; i++)
        empList[i]->ShowSalaryInfo();
        */
}
void EmployeeHandler::ShowTotalSalary() const
{
    int sum = 0;

    /*
    for(int i=0; i<empNum; i++)
        sum += empList[i]->GetPay();
        */

    cout << "salary sum: " << sum << endl;
}
//----------------------------
TemporaryWorker::TemporaryWorker(char* pName, int nPay) : Employee(pName), workTime(0), payPerHour(nPay)
{
    //
}
TemporaryWorker::~TemporaryWorker()
{
    //
}
void TemporaryWorker::AddWorkTime(int nTime)
{
    workTime += nTime;
}
int TemporaryWorker::GetPay() const
{
    return workTime * payPerHour;
}
void TemporaryWorker::ShowSalaryInfo() const
{
    ShowYourName();
    cout << "salary: " << GetPay() << endl << endl;
}
//----------------------------
SalesWorker::SalesWorker(char* pName, int nMoney, double dRatio)
    : PermanentWorkder(pName, nMoney), salesResult(0), bonusRatio(dRatio)
{
    //
}
SalesWorker::~SalesWorker()
{
    //
}
void SalesWorker::AddSalesResult(int nValue)
{
    salesResult += nValue;
}
int SalesWorker::GetPay() const
{
    return PermanentWorkder::GetPay() + (int)(salesResult*bonusRatio);
}
void SalesWorker::ShowSalaryInfo() const
{
    ShowYourName();
    cout << "salary: " << GetPay() << endl << endl;
}

int main()
{
    //직원 관리를 목적으로 설계된 컨트롤 클래스의 객체생성
    EmployeeHandler handler;

    //직원 등록
    handler.AddEmployee(new PermanentWorkder("KIM", 1000));
    handler.AddEmployee(new PermanentWorkder("LEE", 1500));
    handler.AddEmployee(new PermanentWorkder("JUN", 2000));

    //이번 달에 지불해야 할 급여의 정보
    handler.ShowAllSalaryInfo();

    //이번 달에 지불해야 할 급여의 총합
    handler.ShowTotalSalary();

    return 0;
}
